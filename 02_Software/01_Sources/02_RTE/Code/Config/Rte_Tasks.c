/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"
#include "Os.h"

/* SWC Headers. */
#include "Ctrl.h"
#include "Joy.h"
#include "Servo.h"
#include "Buzz.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
   Rte_Start();
   Joy_Init();
   Servo_Init();
   Buzz_Init();
   Ctrl_Init();
}

/**
 * \brief      Application specific OS_FAST_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{
   Buzz_Main();
   Ctrl_Main();
//   Dio_LevelType t_JoyBtnLevel;
//   Adc_ValueGroupType t_JoyXAxisLevel;
//   uint16 us_PwmDuty;
//
//   t_JoyBtnLevel = Dio_ReadChannel(IOHWAB_JOY_STEER_BTN);
//   if (STD_LOW == t_JoyBtnLevel)
//   {
//      /* Buzzing active. */
//      Dio_WriteChannel(IOHWAB_BUZZ_HORN_CTRL, STD_LOW);
//   }
//   else
//   {
//      /* Buzzing inactive. */
//      Dio_WriteChannel(IOHWAB_BUZZ_HORN_CTRL, STD_HIGH);
//   }
//
//   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_X, &t_JoyXAxisLevel);
//   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_X);
//   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_X))
//   {
//      /* Wait for the conversion to finish. */
//   }
//
//   /* Convert from [0...4095] to [0...0x8000]. */
//  //us_PwmDuty = (uint16) (((uint32) t_JoyXAxisLevel * 0x8000UL) / 4095UL);
//  //y = y0 + ((x - x0) * (y1 - y0)) / (x1 - x0)
//   us_PwmDuty = 983 + ((t_JoyXAxisLevel - 0) * (3932 - 983)) / (4095 - 0);
//
//   Pwm_SetDutyCycle(IOHWAB_SERVO_STEER_LEFT, us_PwmDuty);
//   Pwm_SetDutyCycle(IOHWAB_SERVO_STEER_RIGHT, us_PwmDuty);
}

/**
 * \brief      Application specific OS_5MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{
   Joy_Main();
   Servo_Main();

}

/**
 * \brief      Application specific OS_10MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_10MS_TASK)
{
}

/**
 * \brief      Application specific OS_20MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{
}

/**
 * \brief      Application specific OS_100MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_100MS_TASK)     //100ms = 0.1s
{
}

/**
 * \brief      Application specific OS_BACKGROUND_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}

