/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Declares a minimal set of the AUTOSAR ADC standard and implementation data types, exports the global
 *                post build configurations and exports a minimal set of the interfaces for reading ADC groups.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H
#define ADC_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Init.h"
#include "Std_Types.h"
#include "Adc_Cfg.h"

#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------- AUTOSAR Types --------------------------------------------------*/

/** \brief  Holds the numeric IDs of all ADC groups. The IDs are zero based. */
typedef uint8 Adc_GroupType;

/** \brief  Defines the width of one ADC group channel conversion result. */
typedef uint16 Adc_ValueGroupType;

/** \brief  Defines the ADC channel number from a hardware unit. */
typedef uint8 Adc_ChannelType;

/** \brief  Represents the type that defines the current conversion status of an ADC channel group. */
typedef enum
{
   /* The conversion of the specified group has not been started. No result is available. */
   ADC_IDLE,
   /* The conversion of the specified group has been started and is still going on. So far no result is available. */
   ADC_BUSY,
   /* A conversion round (which is not the final one) of the specified group has been finished. A result is available
    * for all channels of the group. */
   ADC_COMPLETED,
   /* The result buffer is completely filled. For each channel of the selected group the number of samples to be
    * acquired is available. */
   ADC_STREAM_COMPLETED
} Adc_StatusType;

/*---------------------------------------------- Implementation Types -----------------------------------------------*/

/** \brief  Represents the configuration of an ADC group, consisting of the hardware unit it is part of and the ADC
 *          hardware channel ID to be written in the first position of the sequence control register. In this
 *          implementation each ADC group consists of (exactly) one ADC channel. */
typedef struct
{
   /** \brief  Base address of the ADC hardware unit. */
   ADC_TypeDef * pt_HardwareUnit;

   /** \brief  ADC channel number from the hardware unit. */
   Adc_ChannelType t_ChannelId;
} Adc_GroupConfigType;

/*---------------------------------------- AUTOSAR Global Configuration Type ----------------------------------------*/

/** \brief  Represents the global ADC configuration type, consisting of a reference to the initialization configuration
 *          and a reference to the configuration of the ADC groups. */
typedef struct
{
   /** \brief  Contains the address of the configuration that is loaded in the initialization function. */
   const Init_Masked32BitsConfigType * kpt_InitConfig;

   /** \brief  Contains the configurations of the ADC groups. */
   const Adc_GroupConfigType * kpt_GroupsConfig;
} Adc_ConfigType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Adc_ConfigType Adc_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Adc_Init(const Adc_ConfigType* ConfigPtr);
extern Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr);
extern void Adc_StartGroupConversion(Adc_GroupType Group);
extern Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* ADC_H */
