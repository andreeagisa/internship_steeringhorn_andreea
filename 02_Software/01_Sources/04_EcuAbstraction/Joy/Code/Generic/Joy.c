/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Joy.c
 *    \author     Andreea Gisa
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Joy.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
Adc_ValueGroupType avg_array_X[3];
Adc_ValueGroupType avg_array_Y[3];
uint8 counter = 0;
uint8 i = 0, j = 0;

Rte_JoyAxisType Joy_AxisX;
Rte_JoyAxisType Joy_AxisY;
Rte_JoyPressType Joy_Btn;

Rte_JoyStsType Joy;

Rte_JoyPressType Btn_Stable_State = RTE_JOY_NOT_PRESSED;


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Joy_Init(void)
{
   uint8 k, m;

   Joy_AxisX = 5000; //50%
   Joy_AxisY = 5000;
   Joy_Btn = RTE_JOY_NOT_PRESSED;

   for (k = 0; k < 3; k++)
   {
      avg_array_X[k] = 2048;
   }

   for (m = 0; m < 3; m++)
   {
      avg_array_Y[m] = 2048;
   }
}

void Joy_Main(void)
{

   Adc_ValueGroupType t_JoyXAxisLevel, t_JoyYAxisLevel, sumX, avgX = 0, sumY, avgY = 0;
   Dio_LevelType Level;
   uint8 it, itt;

   Level = Dio_ReadChannel(IOHWAB_JOY_STEER_BTN);
   if (Level == STD_LOW)
   {
      Joy_Btn = RTE_JOY_PRESSED;
   }
   else
   {
      Joy_Btn = RTE_JOY_NOT_PRESSED;
   }

   if (Joy_Btn != Btn_Stable_State)
   {
      counter++;
      if (counter >= 3)
      {
         Btn_Stable_State = Joy_Btn;
         counter = 0;
      }
   }
   else
   {
      counter = 0;

   }

   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_X, &t_JoyXAxisLevel);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_X);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_X))
   {
      /* Wait for the conversion to finish. */
   }

   avg_array_X[i] = t_JoyXAxisLevel;

   if (i == 2)
   {
      i = 0;
   }
   else
   {
      i++;
   }

   sumX = 0;
   for (it = 0; it < 3; it++)
   {
      sumX += avg_array_X[it];
   }

   avgX = sumX / 3;   //average

   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_Y, &t_JoyYAxisLevel);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_Y);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_Y))
   {
      /* Wait for the conversion to finish. */
   }

   avg_array_Y[j] = t_JoyYAxisLevel;

   if (j == 2)
   {
      j = 0;
   }
   else
   {
      j++;
   }

   sumY = 0;
   for (itt = 0; itt < 3; itt++)
   {
      sumY += avg_array_Y[itt];
   }

   avgY = sumY / 3;   //average

   /* Convert from [0...4095] to [0...10000]. */
   Joy_AxisX = avgX * 10000 / 4095;
   Joy_AxisY = avgY * 10000 / 4095;

   Joy.t_AxisX = Joy_AxisX;
   Joy.t_AxisY = Joy_AxisY;
   Joy.t_Press = Btn_Stable_State;

   Rte_Write_Out_JoySteer(&Joy);

}
