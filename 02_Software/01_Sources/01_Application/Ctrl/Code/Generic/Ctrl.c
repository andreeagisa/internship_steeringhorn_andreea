/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Ctrl.c
 *    \author     Andreea Gisa
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Ctrl.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
   STEERING,
   STEERING_HORN,
   CONFIGURATION
} Ctrl_StatesType;

typedef enum
{
   CONFIG_STEERING_PARALLEL,
   CONFIG_STEERING_ACKERMAN
} Ctrl_Steering_Mode;

typedef enum
{
   EDGE_NO_TRIGGER,
   EDGE_FALLING_TRIGGER
} Ctrl_TriggerEdgeType;

typedef enum
{
   LONG_PRESS_NO_TRIGGER,
   LONG_PRESS_TRIGGER
} Ctrl_TriggerLongPressType;

typedef enum
{
   LEFT,
   RIGHT
} Ctrl_DirectionType;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint16 inner_angle = 0, outer_angle = 0, servo_left, servo_right;

Ctrl_StatesType Ctrl_Sts;

Ctrl_Steering_Mode Config_Steering_Mode;
uint16 Config_Horn_Max_Period;

Ctrl_TriggerEdgeType transition;

Rte_JoyStsType Joy_Data;
Rte_BuzzCtrlType Buzzer;
Rte_JoyPressType Prev_State = RTE_JOY_NOT_PRESSED;

uint16 ctn, delta;

Ctrl_DirectionType directie;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
Ctrl_TriggerEdgeType Falling_Edge(Rte_JoyStsType*);
Ctrl_TriggerLongPressType Long_Pressed(Rte_JoyStsType*);
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
Ctrl_TriggerEdgeType Falling_Edge(Rte_JoyStsType *joystick)
{
   Ctrl_TriggerEdgeType trans = EDGE_NO_TRIGGER;
   Rte_JoyPressType Current_State;

   Current_State = joystick->t_Press;
   if ((Prev_State == RTE_JOY_PRESSED) && (Current_State == RTE_JOY_NOT_PRESSED) && (ctn < 1000))
   {
      trans = EDGE_FALLING_TRIGGER;
   }
   Prev_State = Current_State;
   return trans;
}

Ctrl_TriggerLongPressType Long_Pressed(Rte_JoyStsType *joyst)
{
   Ctrl_TriggerLongPressType Return = LONG_PRESS_NO_TRIGGER;
   if (joyst->t_Press == RTE_JOY_PRESSED)
   {
      if (ctn <= 1000)
      {
         ctn++;
      }
      else
      {
         Return = LONG_PRESS_TRIGGER;
      }
   }
   else
   {
      ctn = 0;
   }
   return Return;
}
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Ctrl_Init(void)
{
   Ctrl_Sts = STEERING;
   Config_Steering_Mode = CONFIG_STEERING_PARALLEL;
   Config_Horn_Max_Period = 500;
}

void Ctrl_Main(void)
{
   Ctrl_TriggerLongPressType LongPressSts;

   switch (Ctrl_Sts)
   {
      case STEERING:
      {
         Rte_Read_InJoy_JoySteer(&Joy_Data);
         transition = Falling_Edge(&Joy_Data);
         LongPressSts = Long_Pressed(&Joy_Data);
         if (transition == EDGE_FALLING_TRIGGER)
         {
            Ctrl_Sts = STEERING_HORN;
         }
         else
         {
            if (LONG_PRESS_TRIGGER == LongPressSts)
            {
               Ctrl_Sts = CONFIGURATION;
            }
            else
            {
               Buzzer.t_Enable = RTE_BUZZ_OFF;
               Rte_Write_OutBuzz_BuzzHorn(&Buzzer);

               //STEERING
               if (Config_Steering_Mode == CONFIG_STEERING_PARALLEL)
               {
                  /* Convert from [0...10000] to [3000...15000]. */
                  inner_angle = 3000 + ((Joy_Data.t_AxisX - 0) * (15000 - 3000)) / (10000 - 0);
                  outer_angle = inner_angle;

                  Rte_Write_OutServo_ServoSteerLeft(&inner_angle);
                  Rte_Write_OutServo_ServoSteerRight(&outer_angle);
               }
               if (Config_Steering_Mode == CONFIG_STEERING_ACKERMAN)
               {
                  if (Joy_Data.t_AxisX > 5000)
                  {
                     delta = Joy_Data.t_AxisX - 5000;
                     directie = RIGHT;
                  }
                  else
                  {
                     delta = 5000 - Joy_Data.t_AxisX;
                     directie = LEFT;
                  }

                  inner_angle = 0 + ((delta - 0) * (6000 - 0)) / (5000 - 0);

                  if ((inner_angle > 0) && (inner_angle <= 500))
                  {
                     outer_angle = 9654 * inner_angle / 10000 + 19 / 1000;
                  }
                  if ((inner_angle > 500) && (inner_angle <= 2500))
                  {
                     outer_angle = 8212 * inner_angle / 10000 + 836 / 1000;
                  }
                  if ((inner_angle > 2500) && (inner_angle <= 4500))
                  {
                     outer_angle = 658 * inner_angle / 1000 + 43264 / 10000;
                  }
                  if ((inner_angle > 4500) && (inner_angle <= 6000))
                  {
                     outer_angle = 6116 * inner_angle / 10000 + 61664 / 10000;
                  }

                  if (directie == LEFT)
                  {
                     servo_left = 9000 + inner_angle;
                     servo_right = 9000 + outer_angle;
                  }

                  if (directie == RIGHT)
                  {
                     servo_right = 9000 - inner_angle;
                     servo_left = 9000 - outer_angle;
                  }

                  Rte_Write_OutServo_ServoSteerLeft(&servo_left);
                  Rte_Write_OutServo_ServoSteerRight(&servo_right);
               }
            }
         }
         break;
      }

      case STEERING_HORN:
      {
         Rte_Read_InJoy_JoySteer(&Joy_Data);
         transition = Falling_Edge(&Joy_Data);
         LongPressSts = Long_Pressed(&Joy_Data);
         if (transition == EDGE_FALLING_TRIGGER)
         {
            Ctrl_Sts = STEERING;
         }
         else
         {
            if (LONG_PRESS_TRIGGER == LongPressSts)
            {
               Ctrl_Sts = CONFIGURATION;
            }
            else
            {
               Buzzer.t_Enable = RTE_BUZZ_ON;
               Buzzer.t_Period = 100 + ((Joy_Data.t_AxisY - 0) * (Config_Horn_Max_Period - 100)) / (10000 - 0);
               Rte_Write_OutBuzz_BuzzHorn(&Buzzer);

               //STEERING_HORN
               if (Config_Steering_Mode == CONFIG_STEERING_PARALLEL)
               {
                  /* Convert from [0...10000] to [3000...15000]. */
                  inner_angle = 3000 + ((Joy_Data.t_AxisX - 0) * (15000 - 3000)) / (10000 - 0);
                  outer_angle = inner_angle;

                  Rte_Write_OutServo_ServoSteerLeft(&inner_angle);
                  Rte_Write_OutServo_ServoSteerRight(&outer_angle);
               }
               if (Config_Steering_Mode == CONFIG_STEERING_ACKERMAN)
               {
                  if (Joy_Data.t_AxisX > 5000)
                  {
                     delta = Joy_Data.t_AxisX - 5000;
                     directie = RIGHT;
                  }
                  else
                  {
                     delta = 5000 - Joy_Data.t_AxisX;
                     directie = LEFT;
                  }

                  inner_angle = 0 + ((delta - 0) * (6000 - 0)) / (5000 - 0);

                  if ((inner_angle > 0) && (inner_angle <= 500))
                  {
                     outer_angle = 9654 * inner_angle / 10000 + 19 / 1000;
                  }
                  if ((inner_angle > 500) && (inner_angle <= 2500))
                  {
                     outer_angle = 8212 * inner_angle / 10000 + 836 / 1000;
                  }
                  if ((inner_angle > 2500) && (inner_angle <= 4500))
                  {
                     outer_angle = 658 * inner_angle / 1000 + 43264 / 10000;
                  }
                  if ((inner_angle > 4500) && (inner_angle <= 6000))
                  {
                     outer_angle = 6116 * inner_angle / 10000 + 61664 / 10000;
                  }

                  if (directie == LEFT)
                  {
                     servo_left = 9000 + inner_angle;
                     servo_right = 9000 + outer_angle;
                  }

                  if (directie == RIGHT)
                  {
                     servo_right = 9000 - inner_angle;
                     servo_left = 9000 - outer_angle;
                  }

                  Rte_Write_OutServo_ServoSteerLeft(&servo_left);
                  Rte_Write_OutServo_ServoSteerRight(&servo_right);
               }
            }
         }
         break;
      }

      case CONFIGURATION:
      {
         Buzzer.t_Enable = RTE_BUZZ_OFF;
         Rte_Write_OutBuzz_BuzzHorn(&Buzzer);

         Rte_Read_InJoy_JoySteer(&Joy_Data);
         inner_angle = 0;
         outer_angle = 18000;
         Rte_Write_OutServo_ServoSteerLeft(&outer_angle);
         Rte_Write_OutServo_ServoSteerRight(&inner_angle);
         transition = Falling_Edge(&Joy_Data);
         (void) Long_Pressed(&Joy_Data);
         if (transition == EDGE_FALLING_TRIGGER)
         {
            Ctrl_Sts = STEERING;
         }
         else
         {
            //CONFIGURATION
            Config_Horn_Max_Period = 500 + ((Joy_Data.t_AxisY - 0) * (1000 - 500)) / (10000 - 0);

            if (Joy_Data.t_AxisX <= 4000)
            {
               Config_Steering_Mode = CONFIG_STEERING_PARALLEL;
            }
            else
            {
               if ((Joy_Data.t_AxisX >= 6000) && (Joy_Data.t_AxisX <= 10000))
               {
                  Config_Steering_Mode = CONFIG_STEERING_ACKERMAN;
               }
            }
         }
         break;
      }

   }

}

