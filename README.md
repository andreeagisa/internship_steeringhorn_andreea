# Internship_SteeringHorn_Startup

Contains the implementation of the 2019 summer Embedded Internship.

Includes a minimal application scenario that:

-controls the red and blue LEDs, same PWM channels to be used for the servomotors, according to the X axis position of the joystick;

-activates the buzzer when the joystick's button is pressed.